﻿using System;
using System.Collections.Generic;
using System.Text;

using FubuMVC.Core.Http;
using FubuMVC.Core.Urls;

#if NUNIT
using NUnit.Framework;
#else
using CThru.BuiltInAspects;
using Gallio.Framework;
using Ivonna.Framework;
using MbUnit.Framework;
using MbUnit.Framework.ContractVerifiers;
using Ivonna.Framework.Generic;
#endif

namespace Chpokk.Tests.Spikes {
	[TestFixture
#if !NUNIT
    // Ivonna
    , RunOnWeb
#endif    
    ]
	public class RelativeUrlInScript {
		[Test]
		public void Test() {
#if !NUNIT
			var session = new TestSession();
			session.AddAspect(new TraceResultAspect(info => info.TargetInstance is ICurrentHttpRequest));
			session.Get("/_content/janrain.js");
#endif
		}
	}
}
