﻿using System.Collections.Generic;
using System.Linq;
using ChpokkWeb.Features.Exploring;
using ICSharpCode.SharpDevelop.Project;
#if !NUNIT
using Arractas;
using MbUnit.Framework;
#else
using NUnit.Framework;
#endif

namespace Chpokk.Tests.Exploring.UnitTests {
	[TestFixture]
	public class ParsingAssemblyReferences
#if !NUNIT
        : BaseQueryTest<ProjectContentWithOneBclReferenceContext, IEnumerable<ReferenceProjectItem>> 
#endif
    {

#if !NUNIT
		[Test]
		public void ReturnsSingleReference() {
			var reference = Result.Single();
			Assert.AreEqual("System.Core", reference.Name);
		}

		public override IEnumerable<ReferenceProjectItem> Act() {
			var parser = Context.Container.Get<ProjectParser>();
			return parser.GetReferences(ProjectContentWithOneBclReferenceContext.PROJECT_FILE_CONTENT);
		}
#endif

	}
}